<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Cache-Control: no-cache');
header('Content-Type: application/json');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Accept, Authorization, X-Requested-With');

/*header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');*/

class UserController extends Controller
{
    public function actionPostAddUser()
    {
        $body = Yii::app()->request->getRawBody();
        $json = CJSON::decode($body);

        $return_data = $this->addUser($json);

        /* response */
        Common::send_api_response(200, CJSON::encode($return_data));
        Yii::app()->end();
    }

    public function addUser($data)
    {
        $item = new User();

        $return_data = $this->addUpdateUser($item, $data);

        return isset($return_data) ? $return_data : [];
    }

    public function actionPostEditUser($id)
    {
        $body = Yii::app()->request->getRawBody();
        $json = CJSON::decode($body);

        $return_data = $this->editUser($id, $json);

        /* response */
        Common::send_api_response(200, CJSON::encode($return_data));
        Yii::app()->end();
    }

    public function editUser($id, $data)
    {
        $item = User::findById($id);

        $return_data = $this->addUpdateUser($item, $data);

        return isset($return_data) ? $return_data : [];
    }

    public function addUpdateUser($item, $data)
    {
        $isNewRecord = $item->isNewRecord ? true : false;

        /* posted data */
        $name = isset($data[ 'name' ]) ? $data[ 'name' ] : '';
        $email = isset($data[ 'email' ]) ? $data[ 'email' ] : '';
        $phone = isset($data[ 'phone' ]) ? $data[ 'phone' ] : '';
        $mobile = isset($data[ 'mobile' ]) ? $data[ 'mobile' ] : '';
        $address = isset($data[ 'address' ]) ? $data[ 'address' ] : '';
        $shortDescription = isset($data[ 'shortDescription' ]) ? $data[ 'shortDescription' ] : '';
        $longDescription = isset($data[ 'longDescription' ]) ? $data[ 'longDescription' ] : '';

        $item->name = $name;
        $item->email = $email;
        $item->phone = $phone;
        $item->mobile = $mobile;
        $item->address = $address;
        $item->shortDescription = $shortDescription;
        $item->longDescription = $longDescription;

        /* validate */
        $item->validate();
        $get_errors = $item->getErrors();


        /* return validation error */
        if ($get_errors) {
            /* validation error as html */
            $values = array_map('array_pop', $get_errors);
            $error_message = implode(' 
            ', $values);

            /* responses */
            $result = 'ERROR';
            $message = $error_message;

        } else if ($item->save()) {
            $result = 'SUCCESS';
            $item = $this->getUserRow($item->id);
            $message = 'User saved successfully!';
        }

        $return_data = [
            'result' => isset($result) ? $result : '',
            'item' => isset($item) ? $item : '',
            'message' => isset($message) ? $message : '',
        ];

        if ($isNewRecord) {
            $data = $this->getAllUser();
            $return_data[ 'listingData' ] = $data;
        }

        return $return_data;
    }

    public function actionGetAllUser()
    {
        $return_data = $this->getAllUser();

        /* response */
        Common::send_api_response(200, CJSON::encode($return_data));
        Yii::app()->end();
    }

    public function getAllUser()
    {
        $model = new User();
        $model = $this->getModelFilter($model);
        $data_provider = $model->search();
        $data_provider->setPagination(['pageSize' => 10]);

        $items = [];
        foreach ($data_provider->getData() as $key => $row) {
            $item = $this->getUserRow($row->id);
            $items [] = $item;
        }

        /* format data */
        $data = [
            'items' => $items,
            'pagination' => Common::getPaginationData($data_provider)
        ];

        return $data;
    }

    public function actionGetUser($id)
    {
        $item = $this->getUserRow($id);

        $return_data = [
            'result' => 'SUCCESS',
            'item' => $item
        ];

        /* response */
        Common::send_api_response(200, CJSON::encode($return_data));
        Yii::app()->end();
    }

    public function getUserRow($id)
    {
        $model = User::findById($id);
        $data = [
            'id' => $model->id,
            'name' => $model->name,
            'email' => $model->email,
            'phone' => $model->phone,
            'mobile' => $model->mobile,
            'address' => $model->address,
            'shortDescription' => $model->shortDescription,
            'longDescription' => $model->longDescription,
            'createdAt' => $model->getCreatedAt(),
        ];
        return $data;
    }

    public function getModelFilter($model)
    {
        $model->name = isset($_GET[ 'filterName' ]) ? $_GET[ 'filterName' ] : '';
        $model->email = isset($_GET[ 'filterEmail' ]) ? $_GET[ 'filterEmail' ] : '';
        $model->phone = isset($_GET[ 'filterPhone' ]) ? $_GET[ 'filterPhone' ] : '';
        $model->mobile = isset($_GET[ 'filterMobile' ]) ? $_GET[ 'filterMobile' ] : '';
        $model->address = isset($_GET[ 'filterAddress' ]) ? $_GET[ 'filterAddress' ] : '';
        $model->shortDescription = isset($_GET[ 'filterShortDescription' ]) ? $_GET[ 'filterShortDescription' ] : '';
        $model->longDescription = isset($_GET[ 'filterLongDescription' ]) ? $_GET[ 'filterLongDescription' ] : '';

        return $model;
    }
}