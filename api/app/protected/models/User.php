<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $address
 * @property string $shortDescription
 * @property string $longDescription
 * @property string $createdAt
 */
class User extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, email, phone, mobile, address, shortDescription, longDescription', 'required'],
            ['email', 'email'],
            ['phone, mobile', 'length', 'min' => 10, 'max' => 10],
            ['phone, mobile', 'numerical', 'integerOnly' => true],

            ['name, email, mobile, address, shortDescription', 'length', 'max' => 120],
            ['shortDescription', 'length', 'max' => 255],
            [
                'id, name, email, phone, mobile, address, shortDescription, longDescription, createdAt',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'shortDescription' => 'Short Description',
            'longDescription' => 'Long Description',
            'createdAt' => 'Created At',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('shortDescription', $this->shortDescription, true);
        $criteria->compare('longDescription', $this->longDescription, true);
        $criteria->compare('createdAt', $this->createdAt, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {

        $this->name = ucwords($this->name);

        if ($this->isNewRecord) {
            $this->createdAt = date('Y-m-d H:i');

        }

        return parent::beforeSave();
    }

    public function getCreatedAt()
    {
        return date('d M Y H:i a', strtotime($this->createdAt));
    }

    public static function findById($id)
    {
        $model = self::model()->findByPk($id);
        if (!$model) {
            $return_data = ['result' => 'ERROR', 'message' => 'No data found'];
            /* response */
            Common::send_api_response(200, CJSON::encode($return_data));
            Yii::app()->end();
        }

        return $model;
    }
}
