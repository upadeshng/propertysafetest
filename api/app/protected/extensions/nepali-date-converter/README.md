include("DateConverter.php");

$converter = new DateConverter();

$converter->setNepaliDate(2074,7,26);
echo $converter->getEnglishYear()."/".$converter->getEnglishMonth()."/".$converter->getEnglishDate();
echo $converter;

$converter->setCurrentDate();
echo $converter->getNepaliYear()."/".$converter->getNepaliMonth()."/".$converter->getNepaliDate();
echo "Weekly day: ".$converter->getDay();
echo $converter->toNepaliString();