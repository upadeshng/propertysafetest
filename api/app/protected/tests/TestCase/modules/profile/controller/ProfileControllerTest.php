<?php

class ProfileControllerTest extends CDbTestCase
{

    protected $fixtures = [
        'users' => 'User',
    ];

    protected function setUp() {
        parent::setUp();
        // your code....
    }

    private function checkUser()
    {
        echo "\n\nStatus of current user:\n";
        echo "--------------------------\n";
        echo "User ID: ".Yii::app()->user->id."\n";
        echo "User Name: ".Yii::app()->user->name."\n";
        if (Yii::app()->user->isGuest)
            echo "There is NO user logged in.\n\n";
        else
            echo "The user is logged in.\n\n";
    }

    public function testChangeemail()
    {

        // set session
        Yii::app()->user->logout();
        $identity = new UserIdentity('doc1@example.net', 'Testing123');
        $identity->authenticate();
        Yii::app()->user->login($identity);

        // log session
        $this->checkUser();


        #$user = User::model()->findByPk(12);
        $user = $this->users('user1');

        $user->setScenario('change_email');
        $user->temporary_email = 'ra122mu@g.com';
        $user->password = md5('Testing123');

        $this->assertTrue($user->save());

        //$this->assertSessionHas($user->save());
        //$this->assertThat('Verification link.');
        //$this->assertSessionHas('flash_notification.level', 'danger');
    }
}