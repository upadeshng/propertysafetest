<?php
/**
 * Created by WebAIS Company.
 * URL: webais.company
 * Developer: alekseyyp
 * Date: 17.12.15
 * Time: 11:03
 */
Yii::import( "application.extensions.YiiMailer.YiiMailer" );

class Mail extends YiiMailer
{

    public $host;
    public $port;
    public $username;
    public $password;
    public $from_email;
    public $from_name;
    public $Mailer = 'smtp';

    private $toEmails = [];
    private $emails   = [];
    private $replyToEmail;
    private $replyToName;
    private $parentId;

    private $emailId;
    private $files = [];

    private $server;

    /**
     * Init
     */
    public function init()
    {
        Yii::import( "application.extensions.postmark-yii-master.Postmark" );
        $this->Host       = $this->host;
        $this->Port       = $this->port;
        $this->SMTPSecure = 'tls';
        $this->SMTPAuth   = true;
        $this->AuthType   = 'PLAIN';
        $this->Username   = $this->username;
        $this->Password   = $this->password;
        $this->clearLayout();

        //$this->server       = EmailThreadUtility::getInboundServer();
        if ( !defined( 'POSTMARKAPP_API_KEY' ) ) {
            define( 'POSTMARKAPP_API_KEY', Yii::app()->params[ 'portmarkToken' ] );

        }
        if ( !defined( 'POSTMARKAPP_MAIL_FROM_ADDRESS' ) ) {

            define( 'POSTMARKAPP_MAIL_FROM_ADDRESS', $this->from_email );

        }
        if ( !defined( 'POSTMARKAPP_MAIL_FROM_NAME' ) ) {

            define( 'POSTMARKAPP_MAIL_FROM_NAME', $this->from_name );

        }

    }

    /**
     * This action sets up necessary configuration for sending email
     * And then email can be sent using send() method
     * @param $view - It is a templates in the views/mail/
     * @param $subject - Email subject
     * @param $data - associative array with data that will be passed to the template
     * @param array $to - an array with recepients, e.g. array("email@example.com" => "Example User", ...)
     * @param string $replyTo - back email
     * @return $this
     * @throws CException
     */
    public function setup( $view, $subject, $data, $to = [], $parentId = null )
    {
        if ( isset( $data[ 'one_way' ] ) && $data[ 'one_way' ] == true ) {
            ## sending email for one-way e.g forgot password, feedback
            $body        = $data[ 'message' ];
            $email_from  = 'contact@medikoma.com';
            $name_from   = $data[ 'name_from' ];
            $reply_email = $data[ 'reply_email' ];
            $file_data = isset($data[ 'file_data' ]) ? $data[ 'file_data' ] : [];

            $postmark = new Postmark();
            $postmark->debug( Postmark::DEBUG_RETURN ); // Otherwise we won't have a response
            $postmark->from( $email_from, $name_from );
            $postmark->addTo( $to[ 'email' ] );
            $postmark->replyTo( $reply_email );
            $postmark->subject( $subject );
            $postmark->messageHtml( $body );

            if(isset($file_data['file']) && isset($file_data['readable_name'])) {
                $postmark->addAttachment($file_data['file'], ['filenameAlias'=>$file_data['readable_name']]);
            }

            $postmark->send();

        }

        return true;
    }


    public function setup_original( $view, $subject, $data, $to = [], $parentId = null )
    {
        $this->toEmails = $to;
        $this->setView( $view );
        $this->setTo( $to );
        $this->setSubject( $subject );
        $this->setData( $data );
        if ( ( $userEmail = UserEmail::getDefault() ) ) {

            $this->setReplyTo( [ $userEmail->email => $userEmail->name ] );
            $this->replyToEmail = $userEmail->email;
            $this->replyToName  = $userEmail->name;

        } else {

            $this->setReplyTo( [ $this->from_email ] );
            $this->replyToEmail = $this->from_email;

        }
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return bool
     */
    public function send()
    {
        $this->emailId = Yii::app()->request->getPost( "emailId" );
        $this->files   = Yii::app()->request->getPost( "files" );

        $this->setFrom( $this->from_email, $this->from_name );

        return parent::send();
    }

    public function PostSend()
    {


        $data = $this->getData();

        if ( !$this->emailId ) {

            $entityType = $this->getView();
            if ( ( $len = strlen( $entityType ) ) > 25 ) {

                $entityType = mb_substr( $entityType, 0, 25, "UTF-8" );

            }
            $this->emailId = EmailThread::createTempThread( isset( $data[ 'entity_id' ] ) ? $data[ 'entity_id' ] : null,
                                                            $entityType );

        }
        /** @var  $mainEmailItemModel EmailThreadItem */
        $mainEmailItemModel = EmailThreadItem::model()->findByPk( $this->emailId );
        /** @var  $user Users */
        $user        = Users::model()->findByPk( Yii::app()->user->id );
        $attachments = $this->GetAttachments();
        $return      = false;

        /*$addresses = array();
        foreach ( $this->toEmails as $email => $name ) {
            $email          = trim($email);
            $name           = trim($name);
            if ( !empty($email) && is_string($email) && strlen($email) > 5 ) {

                $addresses[] = $name . '<' . $email . '>';

            } else {

                $addresses[] = $email = $name;
                $name = null;

            }
        }
        $mainEmailItemModel->to_addresses = implode(";", $addresses);*/

        $threadAttachments = $mainEmailItemModel->emailThreadAttachments;

        /** @var  $mainEmailItemModel EmailThreadItem */
        $mainEmailItemModelFirst = null;
        foreach ( $this->toEmails as $email => $name ) {

            $email = trim( $email );
            $name  = trim( $name );
            if ( !( !empty( $email ) && is_string( $email ) && strlen( $email ) > 5 ) ) {

                $email = $name;
                $name  = null;

            }

            if ( !$mainEmailItemModelFirst ) {

                $mainEmailItemModel->from_name        = $user->name;
                $mainEmailItemModel->from_email       = $user->email;
                $mainEmailItemModel->type             = EmailThreadItem::TYPE_SENT;
                $mainEmailItemModel->date_sent        = date( "Y-m-d H:i:s" );
                $mainEmailItemModel->to_addresses     = $email;
                $mainEmailItemModel->thread->subject  = $this->Subject;
                $mainEmailItemModel->thread->has_sent = 1;
                $mainEmailItemModel->thread->save();

                $attachmentsLinks = [];
                /** @var  $aModel EmailThreadAttachment */
                foreach ( $threadAttachments as $aModel ) {

                    $attachmentsLinks[] = CHtml::link( "Download " . $aModel->filename,
                                                       Yii::app()->createAbsoluteUrl( 'MailThread/index/download', [ 'id' => $aModel->id ] ) );

                }
                $mainEmailItemModelFirst = $mainEmailItemModel;

            } else {

                $threadModel              = new EmailThread();
                $mainEmailItemModel       = new EmailThreadItem();
                $threadModel->has_sent    = 1;
                $threadModel->parent_id   = $mainEmailItemModelFirst->thread_id;
                $threadModel->user_id     = $mainEmailItemModelFirst->thread->user_id;
                $threadModel->subject     = $mainEmailItemModelFirst->thread->subject;
                $threadModel->entity_id   = $mainEmailItemModelFirst->thread->entity_id;
                $threadModel->entity_type = $mainEmailItemModelFirst->thread->entity_type;
                $threadModel->time        = date( "Y-m-d H:i:s" );
                $threadModel->save();

                $mainEmailItemModel->thread_id    = $threadModel->id;
                $mainEmailItemModel->type         = EmailThreadItem::TYPE_SENT;
                $mainEmailItemModel->time         = $threadModel->time;
                $mainEmailItemModel->from_name    = $user->name;
                $mainEmailItemModel->from_email   = $user->email;
                $mainEmailItemModel->date_sent    = $threadModel->time;
                $mainEmailItemModel->to_addresses = $email;
                $mainEmailItemModel->body         = $mainEmailItemModelFirst->body;
                $mainEmailItemModel->save();
                /** @var  $attachmentModel EmailThreadAttachment */
                foreach ( $threadAttachments as $attachmentModel ) {

                    $aModel = new EmailThreadAttachment();
                    $attrs  = $attachmentModel->getAttributes();
                    unset( $attrs[ 'id' ] );
                    $aModel->setAttributes( $attrs );
                    $aModel->item_id = $mainEmailItemModel->id;
                    $aModel->save();

                }

            }
            /** @var $userReceiver Users */
            if ( ( $userReceiver = Users::model()
                                        ->findByAttributes( [ "email" => $email ] ) )
            ) {

                $threadModel              = new EmailThread();
                $threadItemModel          = new EmailThreadItem();
                $threadModel->has_inbox   = 1;
                $threadModel->parent_id   = $mainEmailItemModel->thread_id;
                $threadModel->user_id     = $userReceiver->id;
                $threadModel->subject     = $mainEmailItemModel->thread->subject;
                $threadModel->entity_id   = $mainEmailItemModel->thread->entity_id;
                $threadModel->entity_type = $mainEmailItemModel->thread->entity_type;
                $threadModel->time        = date( "Y-m-d H:i:s" );
                $threadModel->save();

                $threadItemModel->thread_id    = $threadModel->id;
                $threadItemModel->type         = EmailThreadItem::TYPE_INBOX;
                $threadItemModel->time         = $threadModel->time;
                $threadItemModel->from_name    = $user->name;
                $threadItemModel->from_email   = $user->email;
                $threadItemModel->date_sent    = $threadModel->time;
                $threadItemModel->to_addresses = $email;
                $threadItemModel->save();
                /** @var  $attachmentModel EmailThreadAttachment */
                foreach ( $threadAttachments as $attachmentModel ) {

                    $aModel = new EmailThreadAttachment();
                    $attrs  = $attachmentModel->getAttributes();
                    unset( $attrs[ 'id' ] );
                    $aModel->setAttributes( $attrs );
                    $aModel->item_id = $threadItemModel->id;
                    $aModel->save();

                }

            } else {

                $threadItemModel = null;
                $threadModel     = null;

            }

            $postmark = new Postmark();

            $postmark->debug( Postmark::DEBUG_RETURN ); // Otherwise we won't have a response
            $postmark->addTo( $email, $name );
            $postmark->replyTo( EmailThreadUtility::getReplyToEmail( $this->replyToName, $mainEmailItemModel->id ),
                                $this->replyToName );
            $postmark->subject( $this->Subject );
            if ( $this->replyToName ) {

                $postmark->fromName( $this->replyToName );

            }

            $body = $this->Body;

            /*$body = str_replace('{attachments}',  implode('<br>', $attachmentsLinks), $body);*/
            $body = str_replace( 'email_history_id', $mainEmailItemModel->id, $body );
            /* $body .= '<img style="display:none" src="' .
                                 Yii::app()->createAbsoluteUrl('MailThread/index/reademail', array('id' => $mainEmailItemModel->id)) .
                                 '" width=0 height=0 />';*/

            $postmark->messageHtml( $body );

            if ( $attachments && count( $attachments ) ) {

                foreach ( $attachments as $attachment ) {

                    $postmark->addAttachment( $attachment[ 0 ] );

                }

            }
            if ( ( $response = $postmark->send() ) &&
                isset( $response[ 'return' ] ) &&
                ( $data = json_decode( $response[ 'return' ], true ) )
            ) {

                if ( $threadItemModel ) {

                    $threadItemModel->postmark_message_id = $data[ 'MessageID' ];
                    if ( $data[ 'ErrorCode' ] > 0 ) {

                        $threadItemModel->error_code    = $data[ 'ErrorCode' ];
                        $threadItemModel->error_message = $data[ 'Message' ];

                    } else {

                        $return = true;

                    }
                    $threadItemModel->body = $body;
                    $threadItemModel->save( false );

                } else {

                    $return = true;

                }

                if ( $mainEmailItemModel ) {

                    $mainEmailItemModel->postmark_message_id = $data[ 'MessageID' ];
                    if ( $data[ 'ErrorCode' ] > 0 ) {

                        $mainEmailItemModel->error_code    = $data[ 'ErrorCode' ];
                        $mainEmailItemModel->error_message = $data[ 'Message' ];

                    } else {

                        $return = true;

                    }
                    $mainEmailItemModel->body = $body;
                    $mainEmailItemModel->save( false );

                }


            }

        }
        $mainEmailItemModel->body = $body;
        $mainEmailItemModel->save( false );

        return $return;

    }

}