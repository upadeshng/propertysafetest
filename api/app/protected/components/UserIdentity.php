<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $mobile_without_country_code = Common::mobile_without_country_code( $this->username );

        $crt = new CDbCriteria;

        $crt->condition = '(email=:email or mobile=:mobile or id=:id) 
        and (password=:password or password=:pre_encrypted_password) ';

        $crt->params[ ':mobile' ]          = $mobile_without_country_code;
        $crt->params[ ':email' ]           = $this->username;
        $crt->params[ ':id' ]              = $this->username;
        $crt->params[ ':password' ]        = md5( $this->password );
        $crt->params[ ':pre_encrypted_password' ]        = $this->password;

        $member = User::model()->find( $crt );

        if ( $member === null ) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;

        } else {
            /*if ( $member->password !== md5( $this->password ) ) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;

            } else*/ {
                $this->errorCode = self::ERROR_NONE;


                $this->setState( 'id', $member->id );
                $this->setState( 'name', $member->name );
                $this->setState( 'email', $member->email );

                /* encrypted login true/false */
                $encrypted_login = $member->password == md5($this->password) ? false : true;
                $this->setState('password_encrypted_login', $encrypted_login);

            }
        }

        return !$this->errorCode;
    }
    public function api_user_authenticate()
    {
        $mobile_without_country_code = Common::mobile_without_country_code( $this->username );

        $crt = new CDbCriteria;

        $crt->condition = '(email=:email or mobile=:mobile or id=:id) 
        and (password=:password or password=:pre_encrypted_password)';
        
        $crt->params[ ':mobile' ]          = $mobile_without_country_code;
        $crt->params[ ':email' ]           = $this->username;
        $crt->params[ ':id' ]              = $this->username;
        $crt->params[ ':password' ]        = md5( $this->password );
        $crt->params[ ':pre_encrypted_password' ]        = $this->password;

        $user = User::model()->find($crt);


        if ($user === null) {
            return false;

        } else {

            $this->errorCode = self::ERROR_NONE;

            $body = Yii::app()->request->getRawBody();
            $json = CJSON::decode($body);
            $baseUrl = $json['baseUrl'];

            $data = array(
                "name" => $user->name,
                "email" => $user->email,
                "mobile" => $user->mobile,
                "date" => date( 'Y-m-d H:i' ),
                "id" => $user->id,
                "login_type" => 'doctor',
                "baseUrl" => $baseUrl
            );

            /* encode jwt token */
            $token = Yii::app()->JWT->encode($data);

            return $token;

        }

    }
    public function api_patient_authenticate()
    {
        $mobile_without_country_code = Common::mobile_without_country_code( $this->username );

        $crt = new CDbCriteria;

        $crt->condition = '(email=:email or mobile=:mobile) 
        and (password=:password or password=:pre_encrypted_password)';

        $crt->params[ ':mobile' ]          = $mobile_without_country_code;
        $crt->params[ ':email' ]           = $this->username;
        $crt->params[ ':password' ]        = md5( $this->password );
        $crt->params[ ':pre_encrypted_password' ]        = $this->password;

        $patient = Patient::model()->find($crt);


        if ($patient === null) {
            return false;

        } else {

            $this->errorCode = self::ERROR_NONE;

            $data = array(
                "name" => $patient->name,
                "email" => $patient->email,
                "mobile" => $patient->mobile,
                "date" => date( 'Y-m-d H:i' ),
                "id" => $patient->id,
                "login_type" => 'patient',
            );

            /* encode jwt token */
            $token = Yii::app()->JWT->encode($data);

            return $token;

        }

    }


}