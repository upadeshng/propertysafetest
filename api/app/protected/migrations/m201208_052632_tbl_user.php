<?php

class m201208_052632_tbl_user extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_user', [
            'id' => 'pk',
            'name' => 'string',
            'email' => 'string',
            'phone' => 'string',
            'mobile' => 'string',
            'address' => 'string',
            'shortDescription' => 'string',
            'longDescription' => 'text',
            'createdAt' => 'datetime',
        ]);
	}

	public function down()
	{
        $this->dropTable( 'tbl_user' );

        echo "m201208_052632_tbl_user does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}