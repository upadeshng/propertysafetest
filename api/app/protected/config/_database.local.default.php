<?php
return [
    'connectionString' => 'mysql:host=localhost;dbname=property_safe_test',

    'emulatePrepare'   => true,
    'username'         => 'root',
    'password'         => '',
    'charset'          => 'utf8',
];