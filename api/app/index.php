<?php

function env()
{
    $env = getenv('APP_ENV');
    if (empty( $env )) {
        //define('YII_ENV', 'local'); ToDo: Uncomment this if you going to use constants instead of function env()
        return 'local';
    } else {
        //define('YII_ENV', $env); ToDo: Uncomment this if you going to use constants instead of function env()
        return $env;
    }
}



$env = env();

/* cors allow origin for booking button widget */
//if($env == 'dev') {
//    $http_origin = $_SERVER['HTTP_ORIGIN'];
//
//    if ($http_origin == "http://raorizwan.com" // testing widget
//        || $http_origin == "http://medikoma.local"
//        || $http_origin == "http://mmth.org.np" // manmohan memorial hospital -c275
//        || $http_origin == "http://dev.medikoma.com"
//        || $http_origin == "https://medikoma.com"
//        || $http_origin == "http://upadeshbhatta.com"
//        || $http_origin == "http://sumeruhospital.org.np"
//        || $http_origin == "http://nitapolyclinic.com.np"
//        || $http_origin == "http://clinicone.com.np"
//        || $http_origin == "http://schospital.com.np"
//        || $http_origin == "http://naturecarenepal.com.np"
//        || $http_origin == "https://www.happydentalclinicpokhara.com"
//        || $http_origin == "https://dermadentnepal.com"
//        || $http_origin == "http://www.oracare.com.np"
//        || $http_origin == "https://modeleyecenter.com.np"
//        || $http_origin == "http://www.adrc.com.np"
//        || $http_origin == "http://www.vivantadentalhospital.com"
//        || $http_origin == "http://drishtieye.org"
//        || $http_origin == "http://lasercurenepal.com"
//        || $http_origin == "http://www.ifch.com.np"
//        || $http_origin == "http://bluebirdinternationalclinic.com"
//        || $http_origin == "http://www.aavaranskin.com"
//        || $http_origin == "https://dentalife.com.np"
//        || $http_origin == "https://www.civilhealthservice.com"
//        || $http_origin == "https://flamboyant-bassi-609c9a.netlify.com" // for api/pharmacy/SalesbillController.php
//        || $http_origin == "http://bhdc.com.np") {
//
//        header('Access-Control-Allow-Origin: '.$http_origin);
//        header("Access-Control-Allow-Credentials: true");
//        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
//        header('Access-Control-Max-Age: 1000');
//        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, X-Requested-With');
//    }
//}

/* nepali date converter */
$nepali_date_converter = dirname( __FILE__ ) . '/protected/extensions/nepali-date-converter/DateConverter.php';
require_once($nepali_date_converter);

// include global function
$global_function = dirname( __FILE__ ) . '/protected/config/index_global_function.php';
require_once($global_function);
$is_mobile = is_mobile();
if($is_mobile){
    define('IS_MOBILE', true);
}else{
    define('IS_MOBILE', false);
}

// change the following paths if necessary
$yii    = dirname( __FILE__ ) . '/yii/framework/yii.php';

// environment config file
$config = dirname( __FILE__ ) . '/protected/config/main.' . $env . '.php';

// disable debug in production mode
if ($env != 'live'){
    defined( 'YII_DEBUG' ) or define( 'YII_DEBUG', true );
}

// specify how many levels of call stack should be shown in each log message
defined( 'YII_TRACE_LEVEL' ) or define( 'YII_TRACE_LEVEL', 3 );

// show all errors
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );


require_once( $yii );
Yii::createWebApplication( $config )->run();
