import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  withRouter,
  NavLink,
} from 'react-router-dom';
import {
  Modal,
  Button,
  Row,
  Col,
  Form,
  Nav,
  NavDropdown,
  Navbar,
} from 'react-bootstrap';
import logo from './logo.svg';
import './App.css';

import store from './redux/store';
import { Provider } from 'react-redux';

import NavRoute from './config/route';

import setDbHeader from './utils/setDbHeader';

/* importing style */
import styles from './assets/css/styles.css';

import { Redirect } from 'react-router-dom';
import { ProtectedRoute } from './config/protectedRoute';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  return (
    <Provider store={store}>
      <div style={{ marginLeft: '20px', marginRight: '20px', marginTop: '20px' }}>
        <ToastContainer autoClose={5000} />
        <Router basename={'/'}>
          <Route path='/' component={NavRoute} />
        </Router>
      </div>
    </Provider>
  );
};

export default App;
