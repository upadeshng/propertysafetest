import React, { Component } from 'react';

import { connect } from 'react-redux';
import { getUser, postEditUser } from '../../../redux/action/user';

import { Modal, Button, Row, Col, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import Creatable from 'react-select/lib/Creatable';
import AsyncSelect from 'react-select/lib/Async';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import { withRouter } from 'react-router-dom';

class FormEdit extends Component {
  state = {
    id: this.props.match.params.id,
  };

  componentDidMount() {
    this.props.getUser(this.state.id);

  }

  componentWillReceiveProps(recievedProps) {
    const user = recievedProps.user.item;

    if (user) {
      this.setState({
        id: user.id,
        name: user.name,
        email: user.email,
        phone: user.phone,
        mobile: user.mobile,
        address: user.address,
        shortDescription: user.shortDescription,
        longDescription: user.longDescription,
      });
    }
  }

  handleChange = (e) => {
    var name = e.target.name;
    var value = e.target.value;

    if (name === 'name') {
      this.setState({ name: value });
    } else if (name === 'email') {
      this.setState({ email: value });
    } else if (name === 'phone') {
      this.setState({ phone: value });
    } else if (name === 'mobile') {
      this.setState({ mobile: value });
    } else if (name === 'address') {
      this.setState({ address: value });
    } else if (name === 'shortDescription') {
      this.setState({ shortDescription: value });
    } else if (name === 'longDescription') {
      this.setState({ longDescription: value });
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();

    console.log(this.state);
    this.props.postEditUser(this.state.id, this.state, this.props.history);
  };

  render() {
    const { user } = this.props;

    if (!user) {
      return <>Loading..</>;
    }

    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <Row>
            <Col md='8'>
              <div className='form-group'>
                <label>Name: </label>
                <input
                  type='text'
                  name='name'
                  className='form-control'
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </div>
            </Col>
          </Row>



          <Row>
            <Col md='8'>
              <div className='form-group'>
                <label>Email: </label>
                <input
                  type='text'
                  name='email'
                  className='form-control'
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
            </Col>
          </Row>

          

          <Row>
            <Col md='8'>
              <div className='form-group'>
                <label>Phone: </label>
                <input
                  type='text'
                  name='phone'
                  className='form-control'
                  value={this.state.phone}
                  onChange={this.handleChange}
                />
              </div>
            </Col>
          </Row>

          <Row>
            <Col md='8'>
              <div className='form-group'>
                <label>Mobile: </label>
                <input
                  type='text'
                  name='mobile'
                  className='form-control'
                  value={this.state.mobile}
                  onChange={this.handleChange}
                />
              </div>
            </Col>
          </Row>

          <Row>
            <Col md='8'>
              <div className='form-group'>
                <label>Address: </label>
                <input
                  type='text'
                  name='address'
                  className='form-control'
                  value={this.state.address}
                  onChange={this.handleChange}
                />
              </div>
            </Col>
          </Row>

          <Row>
            <Col md='8'>
              <div className='form-group'>
                <label>Short Description: </label>
                <input
                  type='text'
                  name='shortDescription'
                  className='form-control'
                  value={this.state.shortDescription}
                  onChange={this.handleChange}
                />
              </div>
            </Col>
          </Row>

          <Row>
            <Col md='8'>
              <div className='form-group'>
                <label>Long Description: </label>
                <textarea
                  name='longDescription'
                  className='form-control'
                  onChange={this.handleChange}
                  value={this.state.longDescription}
                />
              </div>
            </Col>
          </Row>

          <div className='form-group'>
            <button
              type='submit'
              id='btn_submit'
              onClick={(e) => this.handleSubmit(e)}
              style={{ marginTop: '10px' }}
              className='btn-success btn'
            >
              Submit
            </button>
          </div>
        </form>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.userReducer.user,
});

export default connect(
  mapStateToProps,
  { getUser, postEditUser }
)(withRouter(FormEdit));
