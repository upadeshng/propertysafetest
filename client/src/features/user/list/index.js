import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { connect } from 'react-redux';
import { getAllUser } from '../../../redux/action/user';

import ListTable from './table';
import { Link } from 'react-router-dom';

class Index extends Component {
  state = {
    page: '',
    drugFilter: '',
  };

  componentDidMount() {
    this.props.getAllUser();
  }

  render() {
    const { userListing } = this.props;

    if (!userListing) {
      return <div>Loading..</div>;
    }

    return (
      <div>
        <h4>
          User <small>Listing</small>
        </h4>

        <div className='plane-border-container'>
          <Link
            style={{
              right: '0',
              marginTop: '-10px',
              marginRight: '40px',
              position: 'absolute',
            }}
            className='btn btn-small btn-outline-success'
            to={'/user/add'}
          >
            + ADD
          </Link>

          <ListTable />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userListing: state.userReducer.userListing,
});
export default connect(
  mapStateToProps,
  { getAllUser }
)(Index);
