import React, { Component } from 'react';
import 'react-table/react-table.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getAllUser } from '../../../redux/action/user';

import { Table } from 'reactstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import history from '../../../history';
import PaginateTable from '../../../components/PaginateTable';

class ListTable extends Component {
  state = {
    page: '',
    selectedStaffId: '',
  };

  handlePageChange = (page) => {
    this.setState({ drugGroup_page: page });

    /* get all filters */
    let param = this.state;

    /* send current filter immediately as state can't get current value */
    param.Druggroup_page = page;

    this.reloadGrid(param);
  };

  handleFilterChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;

    /* get all filters */
    let param = this.state;

    if (name === 'filterName') {
      this.setState({ filterName: value });
      param.filterName = value;
    } else if (name === 'filterEmail') {
      this.setState({ filterEmail: value });
      param.filterEmail = value;
    } else if (name === 'filterPhone') {
      this.setState({ filterPhone: value });
      param.filterPhone = value;
    } else if (name === 'filterMobile') {
      this.setState({ filterMobile: value });
      param.filterMobile = value;
    } else if (name === 'filterAddress') {
      this.setState({ filterAddress: value });
      param.filterAddress = value;
    } else if (name === 'filterShortDescription') {
      this.setState({ filterShortDescription: value });
      param.filterShortDescription = value;
    } else if (name === 'filterLongDescription') {
      this.setState({ filterLongDescription: value });
      param.filterLongDescription = value;
    } 

    this.reloadGrid(param);
  };

  reloadGrid = (param) => {
    this.props.getAllUser(param);
  };

  render() {
    const { userListing } = this.props;

    if (!userListing.items) {
      return <div>No data found</div>;
    }

    return (
      <>
        <Table responsive size='sm'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Mobile</th>
              <th>Address</th>
              <th>Short Description</th>
              <th>Long Description</th>
              <th>Created At</th>
              <th style={{ width: '115px' }}></th>
            </tr>
            <tr className='filters' style={{ background: '#E3EAF2' }}>
             
              <th style={{ verticalAlign: 'middle' }}>
                <input
                  type='text'
                  name='filterName'
                  className='form-control'
                  onChange={this.handleFilterChange}
                />
              </th>

              <th style={{ verticalAlign: 'middle' }}>
                <input
                  type='text'
                  name='filterEmail'
                  className='form-control'
                  onChange={this.handleFilterChange}
                />
              </th>

              <th style={{ verticalAlign: 'middle' }}>
                <input
                  type='text'
                  name='filterPhone'
                  className='form-control'
                  onChange={this.handleFilterChange}
                />
              </th>

              <th style={{ verticalAlign: 'middle' }}>
                <input
                  type='text'
                  name='filterMobile'
                  className='form-control'
                  onChange={this.handleFilterChange}
                />
              </th>

              <th style={{ verticalAlign: 'middle' }}>
                <input
                  type='text'
                  name='filterAddress'
                  className='form-control'
                  onChange={this.handleFilterChange}
                />
              </th>

              <th style={{ verticalAlign: 'middle' }}>
                <input
                  type='text'
                  name='filterShortDescription'
                  className='form-control'
                  onChange={this.handleFilterChange}
                />
              </th>

              <th style={{ verticalAlign: 'middle' }}>
                <input
                  type='text'
                  name='filterLongDescription'
                  className='form-control'
                  onChange={this.handleFilterChange}
                />
              </th>

              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {userListing.items.map((item, index) => {
              return (
                <>
                  <tr>
                    <td>{item.name}</td>
                    <td>{item.email}</td>
                    <td>{item.phone}</td>
                    <td>{item.mobile}</td>
                    <td>{item.address}</td>
                    <td>{item.shortDescription}</td>
                    <td>{item.longDescription}</td>
                    
                    <td>{item.createdAt}</td>
                    <td align='right'>
                      <Link
                        className='btn btn-small btn-outline-default'
                        to={'/user/edit/' + item.id}
                        style={{ marginLeft: '5px' }}
                      >
                        Edit
                      </Link>
                    </td>
                  </tr>

                </>
              );
            })}
          </tbody>
        </Table>

        <PaginateTable
          listing={this.props.userListing}
          activePage={this.state.page}
          handlePageChange={this.handlePageChange}
        />
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  userListing: state.userReducer.userListing,
});
export default connect(
  mapStateToProps,
  { getAllUser }
)(ListTable);
