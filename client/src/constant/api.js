// dev
var url = '';

if (process.env.NODE_ENV === 'production') {
  // prod - LOCAL
  var str = window.location.hostname;

  if (window.location.hostname === 'localhost') {
    // accessing localhost
    url = 'http://localhost/PropertySafeProject/api/app';
 
  } else if (str.indexOf('192.168') !== -1) {
    // accessing local network
    url = 'http://'+window.location.hostname+'/PropertySafeProject/api/app';
  
  }
}

export const apiBaseUrl = url;