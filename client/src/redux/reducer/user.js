import {
  SUBMITTING_STARTED,
  SUBMITTING_DONE,
  USER_GET_ALL,
  USER_GET,
  SET_LOADING,
} from '../action/types';

const initialState = {
  user: [],
  userListing: [],
  loading: true,
  submitting: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SUBMITTING_STARTED: {
      return {
        ...state,
        submitting: true,
      };
    }

    case SUBMITTING_DONE: {
      return {
        ...state,
        submitting: false,
      };
    }

    case SET_LOADING: {
      return {
        ...state,
        loading: true,
      };
    }

    case USER_GET_ALL: {
      return {
        ...state,
        userListing: action.payload,
        loading: false,
      };
    }

    case USER_GET: {
      return {
        ...state,
        user: action.payload,
        loading: false,
      };
    }

    default: {
      return state;
    }
  }
};
