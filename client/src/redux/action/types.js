// export constants
export const SET_LOADING = 'SET_LOADING';

export const SUBMITTING_STARTED = 'SUBMITTING_STARTED';
export const SUBMITTING_DONE = 'SUBMITTING_DONE';

// user/staff
export const USER_GET_ALL = 'USER_GET_ALL';
export const USER_GET = 'USER_GET';
