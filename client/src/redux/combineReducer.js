import { combineReducers } from 'redux';

import userReducer from './reducer/user';

export default combineReducers({
  userReducer: userReducer,
});
