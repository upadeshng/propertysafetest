import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  withRouter,
  NavLink,
} from 'react-router-dom';

import NavDropdown from 'react-bootstrap/NavDropdown';
import Nav from 'react-bootstrap/Nav';
import Clock from 'react-digital-clock';
import Moment from 'react-moment';

// user
import UserList from '../features/user/list';
import UserAdd from '../features/user/create';
import UserEdit from '../features/user/edit';


const route = () => {

  return (
    <>
      
      
      {/* user */}
      <Route exact path='/user/edit/:id' component={UserEdit} />
      <Route exact path='/user/add' component={UserAdd} />
      <Route exact path='/user' component={UserList} />
      <Route exact path='/' component={UserList} />

    </>
  );
};

export default route;
