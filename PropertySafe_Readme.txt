This is a `TEST Project for Property Safe`.


## Available Scripts for Frontend & backend based on REST API

## Stack used:
Backend: Yii 1.18
Frontend: ReactJS/Redux
Mysql DB


## Default mysql database user details:
dbname: property_safe_test
username: root
password:

**Note: Please change db name and user details in given file location**
'/Applications/XAMPP/xamppfiles/htdocs/PropertySafeProject/api/app/protected/config/_database.local.php'


## Backend migration to create table
$ cd /Applications/XAMPP/xamppfiles/htdocs/PropertySafeProject/api/app/protected
protected$ /Applications/XAMPP/bin/php  ./yiic migrate up origin/local

**Note: You can also import property_safe_test.sql if you donot wish to run migration**

## Run Project in dev mode
$cd '/Applications/XAMPP/xamppfiles/htdocs/PropertySafeProject/client'
$npm start


## Run Project in prod mode simply browsing 
http://localhost/PropertySafeProject/prod/user
**Note: If there is issue in prod mode, please check project in dev mode**




## RECOMMEND TO RUN IN `dev` MODE to see all the codings

